package com.spring.car;

import java.sql.Date;
import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author sanda
 */
@Entity(name = "TokenValid")
public class TokenValid {
    
    @Id  
//    @GeneratedValue(generator="system-uuid")
//    @GenericGenerator(name="system-uuid", strategy = "uuid")
    /**
     * token est de type java.lang.String car on ne fait aucun arithmetique alors inutile de le mettre en chiffre
     */
    private String token;
//    foreign key avec l'id user
    private String idUser;
    private Date dateFin;

    public TokenValid() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    @Override
    public String toString() {
        return "TokenValid{" + "token=" + token + ", idUser=" + idUser + ", dateFin=" + dateFin + '}';
    }
    
}
