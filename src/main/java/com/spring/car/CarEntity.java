/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.car;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * @author sanda
 * Le mapping représentant une voiture
 */

@Entity
@Table(name = "car")
public class CarEntity {
    
    @Id  @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    /**
     * l'id est de type java.lang.String car on ne fait aucun arithmetique alors inutile de le mettre en chiffre
     */
    private String id;
    
    @Column(name = "marque", nullable = false)
    private String marque;
    
    /* <!-- constructeur  --> */
    public CarEntity() {
    }
    public CarEntity(String marque) {
        this.setMarque(marque);
    }
    /* </-- constructeur  --> */
    
    /* <!-- getter & setter  --> */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }
    /* </-- getter & setter  --> */

    @Override
    public String toString() {
        return "CarEntity : {" + "id=" + id + ", marque=" + marque + '}';
    }
    
    
}
