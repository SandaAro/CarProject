package com.spring.car;

import java.util.List;
import org.springframework.stereotype.Service;

/**
 * @author sanda
 * La classe service contient les metiers de l'entite car
 */
@Service
public class CarService {
    
    /**
     * accès aux données
     */
    private CarRepository carRepository ;

    /**
     * constructeur unique
     * @param carRepository pour l'accès aux données
     * accessible uniquement dans les classes de son package
     */
    protected CarService(CarRepository carRepository) {
        this.setCarRepository(carRepository);
    }

    /**
     * getter de l'attribut carRepository
     * @return CarRepository
     * accessible uniquement dans cette classe service
     */
    private CarRepository getCarRepository() {
        return carRepository;
    }

    /**
     * setter de l'attribut carRepository
     * @return CarRepository
     * accessible uniquement dans cette classe service
     */
    private void setCarRepository(CarRepository carRepository) {
        this.carRepository = carRepository;
    }
    
    /**
     * @return la liste des voitures
     */
    public List<CarEntity> getCars(){
        List<CarEntity> allCarlist = this.getCarRepository().findAll();
        return allCarlist;
    }
    
    /**
     * @param marque non obligatoire, la marque de la nouvelle voiture
     * @return l'instance de la voiture inserée
     */
    public CarEntity create(String marque) {
        CarEntity carTemplate = new CarEntity(marque);
        CarEntity savedCar = this.getCarRepository().save(carTemplate);
        return savedCar;
    }
}
