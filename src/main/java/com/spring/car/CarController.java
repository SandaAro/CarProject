package com.spring.car;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sanda
 * La classe controller pour les voitures ou "car"
 * RestController sert à définir cette classe en tant que Rest
 * "/api/car" est l'url avec laquelle on accède cet API
 */
@RestController
@RequestMapping("/api/car")
public class CarController {
    
    /**
     * Autowired afin d'injecter une instance de Bean dans l'interface CarRepository
     * CarRepository est l'interface qui permet d'accèder à la base de donnée (PostgreSQL)
     */
    @Autowired
    private CarRepository carRepository ;    
    
    /**
     * URL : /api/car/get-all
     * Method: GET
     * @return la liste de tous les voitures sans filtre
     */
    @GetMapping("/get-all")
    public List<CarEntity> getAll(){
        CarService service = new CarService(carRepository);
        return service.getCars();
    }
    
    /**
     * URL : /api/car/create
     * Method: POST
     * @param marque : non obligatoire, lorsqu'elle est introuvable on la definit par "non defini"
     * @return une instance de CarEntity : la nouvelle voiture
     * Insertion d'une nouvelle voiture
     */
    @PostMapping("/create")
    public CarEntity create(
            @RequestParam(required = false, defaultValue = "non defini") String marque) {
        CarService service = new CarService(carRepository);
        return service.create(marque);
    }
    
}
