/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Date;

/**
 *
 * @author sanda
 */
public class Static {
    
    public static boolean afterNow(Date date) throws Exception{
        if(date == null)
            throw new Exception("token invalide");
        java.util.Date utilDate = new java.util.Date();
        return date.after(utilDate);
    }
}
