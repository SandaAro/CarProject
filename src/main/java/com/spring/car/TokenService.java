package com.spring.car;

import java.util.Optional;
import org.springframework.stereotype.Service;
import util.Static;

/**
 *
 * @author sanda
 */
@Service
public class TokenService {
    
    /**
     * accès aux données
     */
    private TokenRepository tokenRepository ;
    private String user;
    
    /**
     * constructeur unique
     * @param tokenRepository pour l'accès aux données
     * accessible uniquement dans les classes de son package
     */
    protected TokenService(TokenRepository tokenRepository) {
        this.setTokenRepository(tokenRepository);
    }

    /**
     * getter de l'attribut tokenRepository
     * @return TokenRepository
     * accessible uniquement dans cette classe service
     */
    private TokenRepository getTokenRepository() {
        return tokenRepository;
    }

    /**
     * setter de l'attribut tokenRepository
     * @return TokenRepository
     * accessible uniquement dans cette classe service
     */
    private void setTokenRepository(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public String getUser() {
        return user;
    }

    private void setUser(String user) {
        this.user = user;
    }
    /**
     * 
     * @param token
     * @return
     * @throws Exception 
     */    
    public boolean valideToken(String token) throws Exception{
        boolean valid = false;
        Optional<TokenValid> retour = this.getTokenRepository().findById(token);
//        get token where token avy @GET
        if(retour.isPresent()){
            TokenValid value = retour.get();
//          comparer si la date limite est apres aujourd'hui
            valid = Static.afterNow(value.getDateFin());
            if(valid){
                this.setUser(value.getIdUser());
            }
            else{
                throw new Exception("Token expiré");
            }
        }
        else{
            throw new Exception("Token invalide");
        }
        return valid;
    }
}
