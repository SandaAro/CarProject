/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.car;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sanda
 */
@RestController
@RequestMapping("/api/comment")
public class CommentController {
    
    @Autowired
    private CommentRepository commentRepository ;
    @Autowired
    private TokenRepository tokenRepository ;
    
    @GetMapping("/get")
    public List<CommentEntity> getAll(@RequestParam String token,@RequestParam String car) throws Exception{
        List<CommentEntity> list = new ArrayList<>();
        TokenService tokenService = new TokenService(tokenRepository);
        if(tokenService.valideToken(token)){
            CommentService service = new CommentService(commentRepository);
            list = service.getComments(car);
        }
        return list;
    }
    
    @PostMapping("/save")
    public CommentEntity save(@RequestParam String token,@RequestParam String car,@RequestParam String comment) throws Exception{
        CommentEntity beanComment = null;
        TokenService tokenService = new TokenService(tokenRepository);
        if(tokenService.valideToken(token)){
            CommentService commentService = new CommentService(commentRepository);
            beanComment = commentService.save(tokenService.getUser(),car,comment);
        }
        return beanComment;
    }
}
