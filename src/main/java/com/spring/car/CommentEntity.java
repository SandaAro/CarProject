/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.car;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author sanda
 */
@Entity
@Table(name = "comment")
public class CommentEntity {
    @Id  
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;
    private String comment;
//    foreign key car
    private String idCar;
//    foreign key user
    private String idUser;
    
    @CreationTimestamp
    private Date instant;

    public CommentEntity() {
    }

    public CommentEntity(String idUser,String idCar,String comment) {
        this.setIdUser(idUser);
        this.setIdCar(idCar);
        this.setComment(comment);
    }
    public CommentEntity(String idCar) {
        this.setIdCar(idCar);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getIdCar() {
        return idCar;
    }

    public void setIdCar(String idCar) {
        this.idCar = idCar;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public Date getInstant() {
        return instant;
    }

    public void setInstant(Date instant) {
        this.instant = instant;
    }

    @Override
    public String toString() {
        return "CommentEntity{" + "id=" + id + ", comment=" + comment + ", idCar=" + idCar + ", idUser=" + idUser + ", instant=" + instant + '}';
    }
    
    
}
